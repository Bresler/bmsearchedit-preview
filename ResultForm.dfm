object frmResultForm: TfrmResultForm
  Left = 0
  Top = 0
  ClientHeight = 387
  ClientWidth = 443
  Caption = 'frmResultForm'
  OnShow = UniFormShow
  BorderStyle = bsDialog
  OldCreateOrder = False
  MonitoredKeys.Keys = <>
  ActiveControl = UniDBGrid1
  DesignSize = (
    443
    387)
  PixelsPerInch = 96
  TextHeight = 13
  object UniDBGrid1: TUniDBGrid
    Left = 8
    Top = 8
    Width = 418
    Height = 299
    Hint = ''
    DataSource = DataSource1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgRowSelect, dgCheckSelect, dgAlwaysShowSelection, dgConfirmDelete]
    ReadOnly = True
    WebOptions.Paged = False
    LoadMask.Message = 'Loading data...'
    Anchors = [akLeft, akTop, akRight, akBottom]
    TabOrder = 0
  end
  object UniButton1: TUniButton
    Left = 165
    Top = 330
    Width = 98
    Height = 41
    Hint = ''
    Caption = 'Select'
    ModalResult = 1
    Anchors = [akBottom]
    TabOrder = 1
  end
  object DataSource1: TDataSource
    Left = 216
    Top = 136
  end
end

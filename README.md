BMUniTriggersButtonEdit
=======================
BMUniSearchEdit is a uniGUI's component based on EXTJS TriggerField.

It shows all records in a new TUniForm from a Table and Field previously specified. The component, using a TFDConnection created by the user, allows the selection of the Table in a dropdown list (TableForSearch in Object Inspector), and automatically update the Field dropdown list (FieldForSearch in Object Inspector). The component works with all Databases supported by FireDAC.


Installation
------------
Open the BMUniSearchEdit project group and install the Design Time package.